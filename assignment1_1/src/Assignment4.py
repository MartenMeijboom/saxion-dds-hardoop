from typing import Dict, List
from Classes import Person, Track, TrackHistory
from Util import readFile
import sys

from map_reduce_lib import *


def mapper(input):
    if Track.supports(input):
        item = Track(input)
        return (item.track_id, item.artist)
    elif TrackHistory.supports(input):
        item = TrackHistory(input)
        return (item.track_id, int(item.person_id))
    elif Person.supports(input):
        item = Person(input)
        return (int(item.id), (item.first_name, item.last_name))
    else:
        return None


def reducer(key_value_entry):
    key, values = key_value_entry

    if isinstance(key, int):
        return key_value_entry
    else:
        final_result = []

        artist_name: str = None
        result: Dict[int, int] = dict()
        for value in values:
            if isinstance(value, int):
                result[value] = result.get(value, 0) + 1
            else:
                artist_name = value

        for k, v in result.items():
            final_result.append((k, v, artist_name))

        return final_result


def second_mapper(input):
    if isinstance(input, list):
        results = []

        for i in input:
            results.append((i[0], (i[1], i[2])))

        return results
    else:
        name = input[1][0]
        return [(input[0], (name[0] + ' ' + name[1]))]


def second_reducer(key_value_entry):
    key, values = key_value_entry

    song_values = list(filter(lambda v: isinstance(
        v, tuple) and v[1] is not None, values))
    person_name = list(filter(lambda v: isinstance(v, str), values))[0]

    song_values = sorted(
        song_values, key=lambda song: song[0], reverse=True)[:1]

    return (key, person_name, song_values)


def run(play_history: list, tracks: list, people: list, debug: bool):

    map_reduce = MapReduce(
        [(mapper, reducer), (second_mapper, second_reducer)], 32)
    result = map_reduce(play_history + tracks + people, 8, debug)

    result.sort(key=lambda r: r)

    original_stdout = sys.stdout

    with open('./src/result.txt', 'a') as f:
        if not debug:
            sys.stdout = f

        for id, name, artists in result:
            for artist in artists:
                print(
                    f'{name} listened most to {artist[1]} with {artist[0]} times')

        sys.stdout = original_stdout

# Uncomment to run independently


folder = './dataset-small'
#folder = './dataset-large'

playHistoryData = readFile(folder + '/playhistory.csv')
trackData = readFile(folder + '/tracks.csv')
personData = readFile(folder + '/people.csv')

run(playHistoryData, trackData, personData, True)
