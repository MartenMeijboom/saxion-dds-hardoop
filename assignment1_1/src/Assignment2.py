from typing import Dict, List
from Classes import Person, Track, TrackHistory
from Util import readFile
import sys

from map_reduce_lib import *


def mapper(input):
    if Person.supports(input):
        item = Person(input)
        return ((item.id), (item.first_name, item.last_name))
    else:
        item = TrackHistory(input)
        return ((item.person_id), (item.hour, 1))


def reducer(key_value_entry):
    key, value = key_value_entry

    person_name: str

    values: Dict[str, int] = dict()

    for val in value:
        if isinstance(val[1], str):
            person_name = val[0] + ', ' + val[1]
        else:
            current_val = values.get(val[0], 0)
            values[val[0]] = current_val + val[1]

    top_hour = sorted(values, key=values.get, reverse=True)[0]

    return (person_name, (top_hour, values.get(top_hour)))


def run(play_history: List, people: List, debug: bool):

    map_reduce = MapReduce([(mapper, reducer)], 32)
    result = map_reduce(play_history + people, 8, debug)

    result.sort(key=lambda r: r)

    original_stdout = sys.stdout

    with open('./src/result.txt', 'a') as f:
        if not debug:
            sys.stdout = f

        for k, v in result:
            print(f'{k}, {v[0]}, {v[1]}')

        sys.stdout = original_stdout

# Uncomment to run independently


folder = './dataset-small'
#folder = './dataset-large'

playHistoryData = readFile(folder + '/playhistory.csv')
peopleData = readFile(folder + '/people.csv')


run(playHistoryData, peopleData, True)
