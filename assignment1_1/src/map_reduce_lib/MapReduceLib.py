import collections
import itertools
import multiprocessing


class MapReduce(object):

    def __init__(self, functions, num_workers=None):
        """
        map_func

          Function to map inputs to intermediate data. Takes as
          argument one input value and returns a tuple with the key
          and a value to be reduced.

        reduce_func

          Function to reduce partitioned version of intermediate data
          to final output. Takes as argument a key as produced by
          map_func and a sequence of the values associated with that
          key.

        num_workers

          The number of workers to create in the pool. Defaults to the
          number of CPUs available on the current host.
        """
        self.functions = functions
        self.pool = multiprocessing.Pool(num_workers)
        self.num_workers = num_workers

    def partition(self, mapped_values):
        """Organize the mapped values by their key.
        Returns an unsorted sequence of tuples with a key and a sequence of values.
        """
        partitioned_data = collections.defaultdict(list)
        for key, value in mapped_values:
            partitioned_data[key].append(value)
        return partitioned_data.items()

    def __call__(self, inputs, chunksize=1, debug=False):
        """Process the inputs through the map and reduce functions given.

        inputs
            An iterable containing the input data to be processed.

        chunksize=1
          The portion of the input data to hand to each worker.  This
          can be used to tune performance during the mapping phase.
        """
        if debug:
            print('=== Mapping to %d mappers with chunk size %d... ===' %
                  (self.num_workers, chunksize))

        counter = 1
        for map_func, reduce_func in self.functions:
            print(f'loop #{counter}')

            # Map and partition
            map_responses = self.pool.map(
                map_func, inputs, chunksize=chunksize)
            map_responses = filter(None, map_responses)
            map_responses = list(map_responses)

            if isinstance(map_responses[0], list):
                new_map_responses = []
                for sublist in map_responses:
                    for item in sublist:
                        new_map_responses.append(item)

                map_responses = new_map_responses

            partitioned_data = self.partition(itertools.chain(map_responses))

            if debug:
                print('=== Mapper returned %d keys ===' %
                      len(partitioned_data))
                print('=== Reducing using %d reducers... ===' %
                      self.num_workers)

            # Reduce
            reduced_values = self.pool.map(reduce_func, partitioned_data)

            inputs = reduced_values

            counter += 1

            if debug:
                print('=== Reducer finished ===')

        return inputs


def process_print(*args):
    print('[' + multiprocessing.current_process().name + ']', 'yellow')
