import csv
from typing import List


def readAndCastFile(fileName: str, T):
    with open(fileName, newline='') as csvfile:

        print(f'Importing {T}: ')

        dialect = csv.Sniffer().sniff(csvfile.read(1024))
        csvfile.seek(0)

        result: List[T] = []

        first = True

        for row in csv.reader(csvfile, dialect, delimiter=',', quotechar='|'):
            if not first:
                result.append(T(row))
            else:
                first = False

        return result


def readFile(fileName: str):
    with open(fileName, newline='') as csvfile:

        dialect = csv.Sniffer().sniff(csvfile.read(1024))
        csvfile.seek(0)

        result = []

        first = True

        for row in csv.reader(csvfile, dialect, delimiter=',', quotechar='|'):
            if not first:
                result.append(row)
            else:
                first = False

        return result
