import sys
from Assignment1 import run as run_assignment_one
from Assignment2 import run as run_assignment_two
from Assignment3 import run as run_assignment_three
from Assignment4 import run as run_assignment_four


from Util import readFile

DEBUG = False

#folder = './dataset-small'
folder = './dataset-large'

personData = readFile(folder + '/people.csv')
playHistoryData = readFile(folder + '/playhistory.csv')
trackData = readFile(folder + '/tracks.csv')

print('---- finished importing -----')

# clear output
original_stdout = sys.stdout

with open('./src/result.txt', 'w') as f:
    sys.stdout = f
    print('Output assignment one')
    sys.stdout = original_stdout

print('Assignment one')

run_assignment_one(playHistoryData, DEBUG)

print('Assignment two')

run_assignment_two(playHistoryData, personData, DEBUG)

print('Assignment three')

run_assignment_three(playHistoryData, trackData, DEBUG)

print('Assignment four')
run_assignment_four(playHistoryData, trackData, personData, DEBUG)

exit
