month_names = ('', 'January', 'February', 'March', 'April', 'May', 'June',
               'July', 'August', 'September', 'October', 'November', 'December')


class Person:
    def __init__(self, args) -> None:
        if(args):
            self.id: int = args[0]
            self.first_name: str = args[1]
            self.last_name: str = args[2]
            self.email: str = args[3]
            self.gender: str = args[4]
            self.country: str = args[5]
            self.dob: str = args[6]

    def __str__(self) -> str:
        return f'id: {self.id}, first name: {self.first_name}, last name: {self.last_name}, email: {self.email}, gender: {self.gender}, country: {self.country}, dob: {self.dob}'

    @staticmethod
    def supports(input):
        if len(input) == 7:
            try:
                attempt = int(input[0])
                return True
            except:
                return False


class TrackHistory:
    def __init__(self, args) -> None:
        if(args):
            self.track_id: str = args[0]
            self.person_id: int = args[1]
            self.date_time: str = args[2]

            history_date_split = self.date_time.split('-')
            self.year = int(history_date_split[0])
            self.month_name = month_names[int(history_date_split[1])]
            self.time: str = history_date_split[2]
            history_time_split = self.time.split(' ')[1].split(':')

            self.hour: str = history_time_split[0]
            self.minute: str = history_date_split[1]

    def __str__(self) -> str:
        return f'track_id: {self.track_id}, person_id: {self.person_id}, date_time: {self.date_time} \n'

    @staticmethod
    def supports(input):
        if len(input) == 3:
            try:
                attempt = int(input[1])
                return True
            except:
                return False


class Track:
    def __init__(self, args) -> None:
        if(args):
            self.track_id: str = args[0]
            self.artist: str = args[1]
            self.title: str = args[2]
            self.length: int = args[3]

    def __str__(self) -> str:
        return f'track_id: {self.track_id}, artist: {self.artist}, title: {self.title}, length: {self.length}s'

    @staticmethod
    def supports(input):
        if len(input) == 4:
            try:
                attempt = int(input[3])
                return True
            except:
                return False
