from typing import Dict, List
from Classes import Person, Track, TrackHistory
from Util import readFile
import sys

from map_reduce_lib import *


def mapper(input):
    if Track.supports(input):
        item = Track(input)
        return ((item.track_id), (item.title, item.artist))
    elif TrackHistory.supports(input):
        item = TrackHistory(input)
        return ((item.track_id), (1, item.hour))
    else:
        return None


def reducer(key_value_entry):
    key, value = key_value_entry

    track_name: str = ""
    artists_name: str = ""

    values: Dict[str, int] = dict()

    for val in value:
        if isinstance(val[0], str):
            track_name = val[0]
            artists_name = val[1]
        else:
            values[val[1]] = values.get(val[1], 0) + val[0]

    return (key, track_name, artists_name, values)


def second_mapper(input):
    times: Dict[str, int] = input[3]
    result = []
    for k, v in times.items():
        result.append((k, (v, input[1], input[2])))

    return result


def second_reducer(key_value_entry):
    key, values = key_value_entry

    values.sort(key=lambda r: r[0], )
    values = sorted(values, reverse=True)[:5]

    return (key, values)


def run(play_history: List, tracks: List, debug: bool):

    map_reduce = MapReduce(
        [(mapper, reducer), (second_mapper, second_reducer)], 32)
    result = map_reduce(play_history + tracks, 8, debug)

    result.sort(key=lambda r: r)

    original_stdout = sys.stdout

    with open('./src/result.txt', 'a') as f:
        if not debug:
            sys.stdout = f

        for k, values in result:
            time_plus_one = str(int(k) + 1)
            if len(time_plus_one) == 1:
                time_plus_one = '0' + time_plus_one

            print(f'top 5 songs between: {k} and {time_plus_one}')
            for v in values:
                print(f'    {v[1]}, {v[2]}, {v[0]}')
            print('')

        sys.stdout = original_stdout

# Uncomment to run independently


folder = './dataset-small'
#folder = './dataset-large'

playHistoryData = readFile(folder + '/playhistory.csv')
trackData = readFile(folder + '/tracks.csv')


run(playHistoryData, trackData, True)
