from typing import List
from Classes import Person, Track, TrackHistory
from Util import readFile
import sys

from map_reduce_lib import *


def mapper(input):
    item = TrackHistory(input)
    return ((item.track_id, item.year, item.month_name), 1)


def reducer(key_value_entry):
    key, value = key_value_entry
    return (key, sum(value))


def run(play_history: List, debug: bool):
    map_reduce = MapReduce([(mapper, reducer)], 32)
    result = map_reduce(play_history, 16, debug)

    result.sort(key=lambda r: r)

    original_stdout = sys.stdout

    with open('./src/result.txt', 'a') as f:
        if not debug:
            sys.stdout = f

        for k, v in result:
            print(f'{k[0]} was played {v} times in {k[2]} {k[1]}')

        sys.stdout = original_stdout


# Uncomment to run independently

folder = './dataset-small'
#folder = './dataset-large'

playHistoryData = readFile(folder + '/playhistory.csv')

run(playHistoryData, True)
