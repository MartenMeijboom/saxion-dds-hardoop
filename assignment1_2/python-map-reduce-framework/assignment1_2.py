#!/usr/bin/python3

import sys
import os.path
import multiprocessing
import string
import operator
from map_reduce_lib import *
from os import listdir
from os.path import isfile, join

files = [f for f in listdir("works/") if isfile(join("works/", f))]

def map_lines_to_words(line):
    output = []
    linenr = line.split("\t")[0]

    line = line.lower().strip()
    ##line = line.translate(str.maketrans(string.punctuation, ' ' * len(string.punctuation)))

    for word in line.split():
        output.append( (word + "@" + linenr, 1) )
    
    return output

def reduce_word_count(key_value_item):
    word, occurances = key_value_item
    return (word, sum(occurances))

def lookup_word(searchvalue, word_counts, file, compact):
    hits = []
    
    for word in word_counts:
        if searchvalue.lower() in word[0].split("@")[0]:
            hits.append(word[0].split("@")[1])
    
    if compact == True:
        if len(hits) == 0:
            return ""
        else:
            return "Within the file " + str(file) + ", the word: " + str(searchvalue.lower()) + ", came up a total of " + str(len(hits)) + " times on lines: \n" + str(hits) + "\n"
    else:
        return "Within the file " + str(file) + ", the word: " + str(searchvalue.lower()) + ", came up a total of " + str(len(hits)) + " times on lines: \n" + str(hits) + "\n"


if __name__ == '__main__':
    print("====================================================================")
    print("Assignment 1_2")
    print("====================================================================")
    print("\n")

    for file in files:
        input_file = open("works/" + file, 'r')
        file_contents = input_file.read().splitlines()

        #############################################################################
        #print(str(file_contents))
        #print("\n")

        # Execute MapReduce job in parallel.
        map_reduce = MapReduce(map_lines_to_words, reduce_word_count, 8)
        word_counts = map_reduce(file_contents, debug=False)
        #print(word_counts)
        
        #THE LOOKUP FUNCTIONALITY
        lookup_wrd = "honeysuckle"
        compact = True
        result = lookup_word(lookup_wrd, word_counts, file, compact)
        if result != "":
            print(result)

        #The output looks the way it does because the shown output in the assignment would add a lot of bulk
        #(the file name for every hit to be precise), this is cleaner imo.
        #Assignment output format given can still be enforced here necessary, let us know if that is the case.
        #############################################################################
