from typing import Dict
from Classes import LogItem
from Util import read_file
import sys

from map_reduce_lib import *

input = read_file()


def mapper(input):
    try:
        item = LogItem(input)
        return ((item.year), (item.month, item.ip, item.id))
    except:
        print(f'error mapping {input}')


def reducer(key_value_entry):
    try:
        key, values = key_value_entry

        results: Dict[str, dict] = dict()

        for val in values:
            new_dict = results.get(val[0], dict())
            new_dict[val[1]] = new_dict.get(val[1], 0) + 1

            results[val[0]] = new_dict

        return (key, results)
    except:
        print(f'error reducing {key_value_entry}')


DEBUG = False

map_reduce = MapReduce([(mapper, reducer)], 64)
result = map_reduce(input, 16, DEBUG)

original_stdout = sys.stdout

with open('./assignment1_3/src/result.txt', 'w') as f:
    if not DEBUG:
        sys.stdout = f

    for key, val in result:
        print(f'{key}:')
        for month, ips in val.items():
            print(f'    {month}:')
            for ip, count in ips.items():
                print(f'        {ip}: {count}')

    sys.stdout = original_stdout

exit
