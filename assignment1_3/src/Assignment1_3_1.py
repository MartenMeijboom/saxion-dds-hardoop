from Classes import LogItem
from Util import read_file
import sys

from map_reduce_lib import *

input = read_file()


def mapper(input):
    item = LogItem(input)
    return (item.ip, item)


def reducer(key_value_entry):
    key, value = key_value_entry

    return (key, len(value))


map_reduce = MapReduce([(mapper, reducer)], 64)
result = map_reduce(input, 16, True)

for key, val in result:
    print(f'{key} - {val}')

exit
