class LogItem:
    def __init__(self, args) -> None:
        if(args):
            try:
                self.ip: str = args[0]

                if len(args[3]) >= 16:
                    self.timestamp: str = args[3]
                else:
                    self.timestamp: str = args[4]

                date_time_split = self.timestamp.split(':')
                date_split = date_time_split[0].split('/')

                self.month = date_split[1]
                self.year = date_split[2]

                self.id = args[-1]
            except:
                print('error')

    def __str__(self) -> str:
        return f'hit from {self.ip} on {self.timestamp}'
